package br.edu.oodb;

public class Usuario {

	private String nome;

	/**
	 * implementação da associacao de classe um-para-um
	 * @return
	 */
	public Senha codigo = new Senha();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
	public Senha getCodigo(){
		return codigo;
	}
	
}
